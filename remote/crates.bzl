"""
@generated
cargo-raze generated Bazel file.

DO NOT EDIT! Replaced on runs of cargo-raze
"""

load("@bazel_tools//tools/build_defs/repo:git.bzl", "new_git_repository")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")  # buildifier: disable=load
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")  # buildifier: disable=load

def raze_fetch_remote_crates():
    """This function defines a collection of repos and should be called in a WORKSPACE file"""
    maybe(
        http_archive,
        name = "raze__darling__0_10_2",
        url = "https://crates.io/api/v1/crates/darling/0.10.2/download",
        type = "tar.gz",
        sha256 = "0d706e75d87e35569db781a9b5e2416cff1236a47ed380831f959382ccd5f858",
        strip_prefix = "darling-0.10.2",
        build_file = Label("//remote/remote:BUILD.darling-0.10.2.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__darling_core__0_10_2",
        url = "https://crates.io/api/v1/crates/darling_core/0.10.2/download",
        type = "tar.gz",
        sha256 = "f0c960ae2da4de88a91b2d920c2a7233b400bc33cb28453a2987822d8392519b",
        strip_prefix = "darling_core-0.10.2",
        build_file = Label("//remote/remote:BUILD.darling_core-0.10.2.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__darling_macro__0_10_2",
        url = "https://crates.io/api/v1/crates/darling_macro/0.10.2/download",
        type = "tar.gz",
        sha256 = "d9b5a2f4ac4969822c62224815d069952656cadc7084fdca9751e6d959189b72",
        strip_prefix = "darling_macro-0.10.2",
        build_file = Label("//remote/remote:BUILD.darling_macro-0.10.2.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__derive_builder__0_9_0",
        url = "https://crates.io/api/v1/crates/derive_builder/0.9.0/download",
        type = "tar.gz",
        sha256 = "a2658621297f2cf68762a6f7dc0bb7e1ff2cfd6583daef8ee0fed6f7ec468ec0",
        strip_prefix = "derive_builder-0.9.0",
        build_file = Label("//remote/remote:BUILD.derive_builder-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__derive_builder_core__0_9_0",
        url = "https://crates.io/api/v1/crates/derive_builder_core/0.9.0/download",
        type = "tar.gz",
        sha256 = "2791ea3e372c8495c0bc2033991d76b512cd799d07491fbd6890124db9458bef",
        strip_prefix = "derive_builder_core-0.9.0",
        build_file = Label("//remote/remote:BUILD.derive_builder_core-0.9.0.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__fnv__1_0_7",
        url = "https://crates.io/api/v1/crates/fnv/1.0.7/download",
        type = "tar.gz",
        sha256 = "3f9eec918d3f24069decb9af1554cad7c880e2da24a9afd88aca000531ab82c1",
        strip_prefix = "fnv-1.0.7",
        build_file = Label("//remote/remote:BUILD.fnv-1.0.7.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__ident_case__1_0_1",
        url = "https://crates.io/api/v1/crates/ident_case/1.0.1/download",
        type = "tar.gz",
        sha256 = "b9e0384b61958566e926dc50660321d12159025e767c18e043daf26b70104c39",
        strip_prefix = "ident_case-1.0.1",
        build_file = Label("//remote/remote:BUILD.ident_case-1.0.1.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__proc_macro2__1_0_24",
        url = "https://crates.io/api/v1/crates/proc-macro2/1.0.24/download",
        type = "tar.gz",
        sha256 = "1e0704ee1a7e00d7bb417d0770ea303c1bccbabf0ef1667dae92b5967f5f8a71",
        strip_prefix = "proc-macro2-1.0.24",
        build_file = Label("//remote/remote:BUILD.proc-macro2-1.0.24.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__quote__1_0_9",
        url = "https://crates.io/api/v1/crates/quote/1.0.9/download",
        type = "tar.gz",
        sha256 = "c3d0b9745dc2debf507c8422de05d7226cc1f0644216dfdfead988f9b1ab32a7",
        strip_prefix = "quote-1.0.9",
        build_file = Label("//remote/remote:BUILD.quote-1.0.9.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__strsim__0_9_3",
        url = "https://crates.io/api/v1/crates/strsim/0.9.3/download",
        type = "tar.gz",
        sha256 = "6446ced80d6c486436db5c078dde11a9f73d42b57fb273121e160b84f63d894c",
        strip_prefix = "strsim-0.9.3",
        build_file = Label("//remote/remote:BUILD.strsim-0.9.3.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__syn__1_0_60",
        url = "https://crates.io/api/v1/crates/syn/1.0.60/download",
        type = "tar.gz",
        sha256 = "c700597eca8a5a762beb35753ef6b94df201c81cca676604f547495a0d7f0081",
        strip_prefix = "syn-1.0.60",
        build_file = Label("//remote/remote:BUILD.syn-1.0.60.bazel"),
    )

    maybe(
        http_archive,
        name = "raze__unicode_xid__0_2_1",
        url = "https://crates.io/api/v1/crates/unicode-xid/0.2.1/download",
        type = "tar.gz",
        sha256 = "f7fe0bb3479651439c9112f72b6c505038574c9fbb575ed1bf3b797fa39dd564",
        strip_prefix = "unicode-xid-0.2.1",
        build_file = Label("//remote/remote:BUILD.unicode-xid-0.2.1.bazel"),
    )
